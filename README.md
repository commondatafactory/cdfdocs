# Documentation
This folder contains the online documentation of Commondatafactory.

Use the following commands to start the watch server:

```bash
npm install
npm start
```

you might need to `npm install -g gatsby`.

Go to http://localhost:8000/ to view the documentation.


```bash
gatsby develop
```
