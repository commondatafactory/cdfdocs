---
title: "Ondermijning"
path: "/docs/ondermijning"
---

# dook.vng.nl

Eerste gemeenten zijn live met dook.vng.nl aan het werk om ondermijning

Op pleio is een groep: https://kennisnetwerkdata.pleio.nl/ thema ondermijning.

vragen? abel.koppert@vng.nl

Samen met gemeenten, software engineers, experts, cartografen
en andere energietransitie-initiatieven werken we aan deze voorziening.

Door middel van data-beschikbaarheid, vragen, gemeentelijke taken, en engineering
capaciteit proberen we zo effectief mogelijk het datagedreven werken
te bevorderen.
