---
title: "Drukte"
path: "/docs/drukte"
---

Door Corona is het hebben van een drukte-indicatie zeer urgent
geworden. Steden waren al op zoek naar goede indicatoren van drukte
om bijvoorbeeld de afval en straat-reiniging beter aan te sturen.

Er is een beproeving gestart met het CTO team van Amsterdam om
met behulp van Google data een actuele indicatie van
drukte-inzicht te krijgen in het effect van maatregelen.

## Beschibare gegevens

De gegevens die beschikbaar zijn, zijn de drukte percentages
voor een locatie. Er zijn voor ons geen persoonsgegevens
beschikbaar. De percentages zeggen iets over 1 locatie en
percentages kunnen niet vergeleken worden. Als het Vondelpark
van 80 naar 90 procent drukte gaat dan gaat het over veel meer
mensen dan waneer het Beatrix Park van 100 naar 110 procent gaat.

Het is een mooie extra indicatie over drukte, anders dan het
'voelt' druk op straat.

Dit is een van de vele potentiele databronnen die iets over drukte
zeggen in een gemeente. Andere bronnen zijn
bijvoorbeeld OV-data, stoplicht-data inzicht op de snelwegen, en
tel camera's. Geen van de bronnen zal een compleet en goed
beeld geven. Daarnaast zijn er ethische afwegingen over wat wel of niet
kan en mag met dit type data.

Voor meer informatie: Tamas Erkelens op amsterdam.nl.
