---
title: "Externe bronnen"
path: "/docs/externbron"
---

Naast het onstluiten van eigen data maken wij ook gebruik van externe Web mapping services (WMS). Hier een overzicht van de gebruikte endpoints:

# Koppelkansen

WMS Klimaat effect atlas

    https://apps.geodan.nl/public/data/org/gws/YWFMLMWERURF/kea_public/wms?request=GetCapabilities&service=WMS

WMS RIVM

    https://data.rivm.nl/geo/alo/wms?request=GetCapabilities&service=WMS

# Ondergrond

WMS Liander - Elektriciteitsnetten

    https://geodata.nationaalgeoregister.nl/liander/elektriciteitsnetten/v1/wms

WMS Rioned - Stedelijk Water en Riolering

    https://geodata.nationaalgeoregister.nl/rioned/gwsw/wms/v1_0


# Bestemmingsplannen

WMS Ruimtelijke plannen WRO

    https://geodata.nationaalgeoregister.nl/plu/wms?request=GetCapabilities&service=WMS
