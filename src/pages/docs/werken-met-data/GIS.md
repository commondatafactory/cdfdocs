---
title: "GIS / QGIS"
path: "/docs/gis"
---

## Zelf aan de slag met GIS.

Mocht u zelf data willen combineren,
of eigen datasystemen willen koppelen en
meerdere combinaties van data willen maken
dan kunt u ook werken met een GIS pakket.

GIS staat voor Geographic Information System
Arcgis en Qgis zijn hier voorbeeldden van.

Om u of uw geo expert snel op weg te helpen hebben we
een QGIS project klaar gezet. QGIS is gratis en
open-source. Versie 3.10 van Qgis is gebruikt voor het maken.

DEGO data gelijk in Qgis.

[kant-en-klaar qgis project](https://files.commondatafactory.nl/qgis/DEGO-2023-08.qgz)


De kleuren en styling die hier in zitten volgen
de viewer maar alles kunt u nu aanpassen.

Documentatie van de energie data [Energie attributen documentatie](/docs/energie/wfs)
