---
title: "Iteratieve Vooruitgang"
path: "/over/history"
---

### Geschiedenis.

Voor het publiceren van werkzaamheden is S.J Preeker begonnen aan
commondatafactory om als een datavoorziening de Commonground principes te verspreiden
in andere projecten.

Data Software Engineer S.J.Preeker heeft jaren gewerkt bij datapunt Amsterdam.
Stephan kwam halverwege 2019 via het NLX.io (team nu FSC NLX) in de VNG terecht.

In Amsterdam deed Stephan vergelijkbare data werkzaamheden voor gemeente land
en kon met die ervaring overstappen naar de VNG en het toenmalige energie team.

De in 2019 gemaakte Kaart Proof of Concept  (POC) ziet u hieronder.
Hier ziet u gebouwen met kleuren die verschillen in energie gebruik weergeven

![2019poc](./POC-2020-02-12-10-14-23.png)

Met de in 2019 nieuwste technieken en "zo zou ik het met de kennis van nu doen" is
aan de eerste versie met veel energie en plezier aan gewerkt. En met success!

Hierna is Stephan onder de noemer van commondatafactory.nl begonnen met het uitwerken
en publiceren van het gemaakte werk. Net als bij datapunt Amsterdam wordt er aan datasets
gewerkt die inzetbaar zijn voor verschillende themas. Nu zijn dat

- DEGO (Data Energie Gebouw de Omgeving)
- DOOK (Data Onregelmatigheden Op de Kaart).

Een beeld van de ontwikkelingen de afgelopen jaren bij de commondatafactory.nl ziet u hieronder.
Commondatafactory wil data werkzaamheden doen
met de commonground principes als uitgangspunt.
Delen, Hergebruiken, Open Source en samenwerken.

Nu met de plannen voor een Data werkplaats lijkt het verwerken van data voor verschillende
themas steeds meer momentum te krijgen.  Het hands-on direct waarde aan gemeente medewerkers
leveren is cruciaal! Het voldoen aan de informatie behoefte van gemeente medewerkers is erg
lastig. Onderandere door de veelheid van taken, veelheid van data, ouderwetse wetgeving,
legacy software en een versnipperd landschap van databronnen en organisaties die wel elkaars
data maar niet elkaars problemen hebben is samenbrengen van informatie een monniken werk.

Andere engineers hebben hier veel profeit van in de toekomst met snellere en betere data
gedreven beslissingen.

In 2020 kwam Niene Boeien, Paul Suijkerbuik, Gerdien van Vrede aanschuiven.
Alleen Stephan is nog over van het oude energie team. Er zijn diverse mensen die tijdelijk
even hun expertise geleend hebben. Designers, Product Owners, Haven experts etc.

Het is mooi om de iteratieve vooruitgang te kunnen laten zien
aan de hand van screenshot en tussentijdse plaatjes.


Begin:2020

![POC](./2019energie.png)

versie uit 2020-07 Inmiddels is weer veel verandert.

![2020-06-versie](./2020-07-29-12-49-07-oudeversie.png)

cbs data met gaten voor verwerking, na de verwerkingen hebben
we een gevulde kaart. (2020-06)

![cbsdata](./2020-06-0317-16-33-cbs.png)

2020 Begin van corona crisis een poc om met google data drukte indicatie te maken

![drukteradara](./coronapoc-2020-05-04-15-30-18.png)

2020-07 maatwerk voor Rotterdam

![maatwerk voor rotterdam](./2020-07-30-14-30-15-maatwerkrotterdam.png)


De realiteit van software:
![xkcd](./xkcd-moderninfrastructure.png)


S2 Geodata selectie maakt het mogelijk om bijzonder grote aantallen data geografish
te analyseren met dataselectie en voor het wijkpaspoort. Achter de schermen werkt
het ongeveer zo. (techniek van google engineers)

![s2](./2020-11-24-10-46-04-S2-geoselectie.png)


Voorbeeld selectie rekenkracht 422.000 items regio denhaag in een paar miliseconden.

![geoselectie](./2020-12-09-16-46-34-grooteselectie.png)


Met bovenstaande tools kon het Wijkpaspoort 2020-10 gerealiseerd worden

![wijkpaspoort](./2020-10-26-18-04-05-wijkpaspoort.png)

Energie data wordt gepubliceert op postcode gebied. Dit is anoniem maar niet heel handig.

![postcodegebieden](./postcodegebieden.png)

Postcode gebieden zijn soms wel erg groot.
![poscodeproblemen](./vb1-postcode-problemen.png)


Bag3D gebouwhoogte

![bag3d](./2021-05-26-16-18-03-bag3d.png)
