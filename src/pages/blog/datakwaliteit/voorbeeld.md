---
title: "Data kwaliteit,  een voorbeeld"
path: "/blog/voorbeeld"
---

We willen graag datagedreven werken maar dan moet je wel terdege op de hoogte zijn
van de datakwaliteit en wat er wel en niet geconcludeerd kan worden met data. We willen
verder dan "mijn mening" of "ik vind". Data goed interpreteren is niet eenvoudig.

## Moeilijkheden.

De volgende zaken kunnen het werken met data belemmeren.

- Data wordt niet voor beleid verzameld, maar om uitvoerings taken te kunnen doen.

Hier heb ik twee voorbeelden van.

1) Parkeerscan auto's scannen niet buiten de rand van
betaald parkeren gebieden. Omdat ze alleen binnen betaal zones bleven
kon de parkeerdienst niet zien dat net buiten
een parkeergebied het bijzonder druk is als daar gratis parkeren mogelijk is.
Bewoners halen hun autos uit garages om toch maar een plek te hebben voor hun bezoek. Hierdoor
werd het nog drukker. Aan de beschikbare data kan dit gedrag niet gezien worden.

2) RIONET is al jaren bezig om een grote database te maken van alle assets met betrekking tot
het Riool. De kleinere deelgebieden zouden hetzelfde moeten gaan werken om alle data samen te
voegen maar dit krijgt geen prioriteit. De lokale uitvoering krijgt prioriteit en het grote
overzicht over all het Riool in Nederland komt maar moeilijk van de grond.

## AVG / Privacy moeilijkheiden.

Wanneer is een 'gegeven' een privacy gegeven? Is een gas-aansluiting niet net zoiets als de kleur
van een huis of de groote en hoogte van een huis? Het oppervlak, bouwjaar en geschatte hoogte
zijn online gepubliceerd van elk gebouw maar de aanwezigheid van een gas-aansluiting is niet eenvoudig
online te vinden. Wij hebben gasaanlsuitingen / EANcodes met moeite verzameld en beschikbaar gemaakt
maar dit zou eigenlijk niet nodig hoeven zijn.

Het energie verbruik van een huis / object is privacy gevoelig. Met slimme meter verbruiks gegevens
zou afgleid kunnen waneer iemand thuis is. Maar is het Standaard jaarverbruik ook privacy gevoelig?

Door het toenemen van 0-op-de-meter huizen wordt het steed moeilijker
om iets over de bewoners te zeggen door naar verbruiks-data te kijken. Er wordt opgewekt en opgeslagen
ook als bewoners niet thuis zijn.


## Dataset resolutie

Hoe gedetaileerd is een dataset? Als voorbeeld nemen we de wijk Hijkerveld in Rotterdam

Volgens de kleinverbruik gegevens die met postcode gebieden werken lijkt het Hijkerveld
gas te gebruiken.

## Zit hier nu gas?

![Image of Gasverbruik P6](./rdamgas.png)

Kijken we naar de EAN codes die kleinverbruik gas-aansluitingen weergeeft dan zien we
geen gas.  EAN codes heeft een resolutie per pand.
Dan zien we dat deze gebouwen helemaal geen gas verbruiken.


![Image of Gas aansluitingen](./rdamgeengas.png)

## Postcode gebieden

De oorzaak is dus de resolutie van de data. Netbeheerders werken met postcode 6 gebieden
en hierdoor worden gebouwen die geen gas aansluiting hebben wel als gas-gebruikend aangemerkt

Het zou echter nog kunnen dat er blokverwarming is op gas met een groot-verbruikers aansluiting
deze zien we niet terug met de EAN codes. Dus het Hijkerveld heeft stadsverwarming hebben maar
deze kan nogsteeds gas-gestookt zijn.

![Image of Gas P6 gebieden](./postcodegebied.png)

## Conclusie

Het hebben van kleinverbruik data per postcode 6 is de een mooi voorbeeld hoe
data alleen niet genoeg is. Het gesprek over de data, de kwaliteit en de resolutie
en wat er wel en niet van afgeleid kan worden is erg belangrijk.

Toets uw eigen wijken en buurten en kijk of het allemaal lijkt te 'kloppen'.

[Datavoorziening Energie Gebouwde Omgeving](https://dego.vng.nl)


