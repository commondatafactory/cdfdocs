#!/usr/bin/env bash

# todo replace this with flux

set -u   # crash on missing env variables
set -e   # stop on any error
set -x   # print what we are doing

git pull

TAG=`git rev-parse --short HEAD`

docker build -t registry.gitlab.com/commondatafactory/cdfdocs:$TAG$1 .
docker push registry.gitlab.com/commondatafactory/cdfdocs:$TAG$1

cd ./helm

kubectl config use-context microk8s # pinniped-azure-data-prod

helm update cdfdocs . -n cdf-prod --values values.prod.yaml --set-string image.tag=$TAG$1

# see progress
kubectl -n cdf-prod get pods -w
